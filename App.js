import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, TouchableHighlight, Image } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { StatusBar } from 'react-native'

class StartScreen extends React.Component {
 static navigationOptions = {
   header: { visible:false },

 };
 render() {
   const { navigate } = this.props.navigation;
   return (

     <View style={styles.container}  >

<View style={styles.containertwo}   >

     <Text style={styles.title} >SASA FIX</Text>
     <Text style={styles.titletwo} >Car Fix? We got this!</Text>
</View>
     <View style={styles.containerthree}  >
<TouchableHighlight style ={styles.button}    onPress={() => navigate('Account')}  >
<Text>Login</Text>
</TouchableHighlight >

<TouchableHighlight style ={styles.button}   >
<Text>Register</Text>
</TouchableHighlight >


</View>
   </View>
   );
 }
}



class AccountScreen extends React.Component {
 // Nav options can be defined as a function of the screen's props:
 static navigationOptions = ({ navigation }) => ({
   title: 'Sasa Fix',
 });
 render() {
   // The screen's current route is passed in to `props.navigation.state`:
   const { params } = this.props.navigation.state;
   return (
     <View style={styles.containeraccount}  >
     <Text style={styles.titleacctwo}>
     How can we serve you today?
     </Text>

     <View style={styles.containerfive}  >

     <TouchableHighlight>
     <Image

style={styles.imageicons}
        source={require('./iconone.jpg')}
      />

     </TouchableHighlight>
     <TouchableHighlight>
     <Image

style={styles.imageicons}
        source={require('./icontwo.jpg')}
      />

     </TouchableHighlight>
     <TouchableHighlight>
     <Image

style={styles.imageicons}
        source={require('./iconthree.jpg')}
      />

     </TouchableHighlight>
          </View>
          <View style={styles.containeracctwo}   >

               <Text style={styles.titleacc} >SASA FIX</Text>

          </View>
     </View>
   );
 }
}






const  SimpleAppNavigator = StackNavigator({
 Start: { screen: StartScreen },
 Account: { screen: AccountScreen },

}, { headerMode: 'none' });

const AppNavigation = () => (
 <SimpleAppNavigator  />
);

export default class App extends React.Component {
 render() {
   return (
       <AppNavigation/>
   );
 }
}








const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#64DD17',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containeraccount: {
    flex: 1,
    backgroundColor: '#0D47A1',
    alignItems: 'center',
    justifyContent: 'center',

    flexDirection: 'column',

  },

  containertwo: {




    backgroundColor: '#64DD17',
    alignItems: 'center',
    justifyContent: 'center',
height:80,
width:250,
marginTop:100,

        borderWidth: 5,
borderColor:'#FFFF'
  },

  containeracctwo: {
  backgroundColor: '#0D47A1',
    alignItems: 'center',
    justifyContent: 'center',
height:60,
width:150,
marginTop:100,

        borderWidth: 5,
borderColor:'#FFFF'
  },
  containerthree: {



flexDirection: 'row',
marginTop:50,

    backgroundColor: '#64DD17',
    alignItems: 'center',



  },


  containerfour: {


  marginTop:50,
flexDirection: 'row',
  alignItems: 'center',


    backgroundColor: '#01579B',





  },
  containerfive: {



  flexDirection: 'row',
  marginTop:50,

    backgroundColor: '#0D47A1',




  },




 title: {

        fontSize: 65,

        color:'#FFFF',





    },

     titleacc: {

            fontSize: 40,

            color:'#FFFF',
},
titleacctwo: {

       fontSize: 20,
       color:'#FFFF',
},

 titletwo: {
        fontSize: 20,
        marginBottom: 10,






    },

 button: {


borderWidth: 2,
borderColor:'#e0e0e0',
margin: 35,
padding:3,
width: 80,
alignItems: 'center',
    justifyContent: 'center'




  },

    imageicons: {
      borderRadius: 25,
   width: 50,
   height: 50,
   margin:20





       }



});
